require_relative 'SYK'
require_relative '../config/init'
require 'tty-link'
display = SYK::Display.new
PROMPT.on(:keyescape) { exit }

PROMPT.on(:keypress) do |event|
  if 'ctrl_b'.eql?(event.key.name.to_s)
    display.main_menu
    LOGGR.error(' Pressed ctrl+b')
  end
end

display.main_menu
