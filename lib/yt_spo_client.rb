require_relative 'spotify_client'
require_relative 'yt_client'
require_relative 'main_prompt'
require_relative '../config/init'
require_relative 'saved_url'

class Yt_spo_client
  def initialize
    @spotify_client = Spotify_client.new
    @yt_client = Yt_client.new
  end
  def convert_spotify_playlist_into_youtube_playlist(url_of_playlist:, title:, description:)
    response = @spotify_client.get_tracks_from_url(url_of_playlist: url_of_playlist)
    return [response[0], response[1]] if !response[0]

    created, id_of_playlist = @yt_client.create_yt_playlist(description, title: title)
    return [created, id_of_playlist] if !created
    progress_bar = TTY::ProgressBar.new("Creating #{title} [:bar]", total: response[1].size)

    response[1].reverse.each do |track|
      @main_prompt = MainPrompt.new if @main_prompt.nil?
      @main_prompt.display_main_prompt
      puts "Putting #{track} into playlist"
      progress_bar.advance
      found, id_of_video = @yt_client.search_track(full_name_of_track: track)
      return [found, id_of_video] if !found

      @yt_client.add_to_playlist(playlist_id: id_of_playlist, track_id: id_of_video)
    end

    url = "https://www.youtube.com/playlist?list=#{id_of_playlist}"
    SavedUrl.new.writing_to_file(url: url, description: title)

    [response[0], url]
  end
  def random_meme_from_youtube
    @yt_client.random_meme
  end
  def random_recommended_music
    @spotify_client.get_random_track_from_recommended_playlist
  end
end
