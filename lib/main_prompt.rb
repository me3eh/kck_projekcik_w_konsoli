require_relative '../config/init'

class MainPrompt
  def clean_screen
    system ("clear") || system("cls")
  end
  def banner
    centered = TTY::Screen.width / 3
    tab = CURSOR.move(centered, 0)
    clean_screen
    puts tab + PASTEL.green(".oooooo..o") + PASTEL.red(" oooooo   oooo ") + PASTEL.magenta("oooo    oooo")
    puts tab + PASTEL.green("d8P'    `Y8  ") + PASTEL.red("`888.   .8'  ") + PASTEL.magenta("`888   .8P'  ")
    puts tab + PASTEL.green("Y88bo.        ") + PASTEL.red("`888. .8'    ") + PASTEL.magenta("888  d8'")
    puts tab + PASTEL.green("`\"Y8888o.     ") + PASTEL.red("`888.8'     ") + PASTEL.magenta("88888[      ")
    puts tab + PASTEL.green("     `\"Y88b     ") + PASTEL.red("`888'      ") + PASTEL.magenta("888`88b.    ")
    puts tab + PASTEL.green("oo     .d8P      ") + PASTEL.red("888       ") + PASTEL.magenta("888  `88b.")
    puts tab + PASTEL.green(" 8\"\"88888P'     ") + PASTEL.red("o888o     ")+ PASTEL.magenta("o888o  o888o ")
    puts "\n\n"
    puts tab + PASTEL.green("spotify\t   ") + PASTEL.red("youtube\t") + PASTEL.magenta("konwerter")
  end
  def display_main_prompt
    clean_screen
    banner
  end
end