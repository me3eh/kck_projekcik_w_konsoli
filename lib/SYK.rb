# frozen_string_literal: true

require_relative "SYK/version"
require 'pastel'
require 'tty-link'
require_relative 'yt_spo_client'
require_relative '../config/init'
require_relative 'saved_url'
require 'clipboard'

HELP_STRING = TTY::Link.link_to("\nlook at this example of obtaining playlist link from desktop client\n",
                                "https://cdn.discordapp.com/attachments/905167631554842714/"+
                                  "905170034962333696/how_to_obtain_link_from_desktop_version.gif") +
  TTY::Link.link_to("\nor at this example of receiving playlist link through web browser client:\n",
                    "https://cdn.discordapp.com/attachments/905167631554842714/" +
                      "905171860076003418/how_to_obtain_link_from_browser_version.gif.gif")
INFORMATION_ABOUT_GENERATING = "Press 'a' to generate one more time and any other key to back to main menu"
INFORMATION = "                            Page 1\n" +
    " You only need link to the playlist/album from spotify.\n" +
    "App will transfer spotify tracks and put them into youtube ones,\n" +
    " giving you a link to get to them. Format of help box message does not\n" +
    " let me share hyperlinks but after you choose option 1 \n" +
    "everything will be clear\n",
    "                            Page 2\n" +
      " Get random music recommendation from developer of this app\n" +
      "(gRuby recommends). Simply click on the option and you \n" +
      "will be given link redirecting to spotify application",
    "                            Page 3\n"+
      "Get some laugh with video recommendation from developer\n" +
      " of this app (gRuby laughs). Simply click on the option and you will be given\n" +
      "link redirecting to youtube application\n",
    "                            Page 4\n" +
      "This option let you choose and copy url of previously \n" +
      "generated playlists. You can delete that file and after \n" +
      "next convertion, file will be created with new content. \n" +
      "Also delete it, when there's too many of options ;)",
    "\nNote that in smaller resolutions of terminal application might\n" +
      "not work so well (url forced cutting, not readable words).\n" +
      "Also the first option (converter) is limited by quota count, so if there's\n" +
      "too many requests made in day, you need to wait or change files from folder switch\n" +
      ". Sorry, Google have rights to do this"

module SYK
  class Error < StandardError; end
  class Display
    def initialize
      @client = Yt_spo_client.new
    end
    def main_menu
      while true
        main_prompt
        help_column
        choice_redirection(main_menu_choice)
      end
    end

    def main_menu_choice
      PROMPT.select("Select your option: ",
                    cycle: true) do |menu|
        menu.enum "."
        menu.choice "Convert Spotify playlist into Youtube playlist", 1
        menu.choice "Get random generated video from youtube", 2
        menu.choice "Get recommended song from spotify", 3
        menu.choice "Help", 4
        menu.choice "Copy the url", 5

        menu.choice "exit", 6
      end
    end
    def choice_redirection(choice)
      case choice
      when 1
        interface_for_converting_spotify_to_youtube_playlist
      when 2
        interface_for_random_meme
      when 3
        interface_for_recommended_music
      when 4
        help
      when 5
        choose_and_copy_converted_urls
      when 6
        clean_screen
        box(what_type_of_box: "completion", caption_inside: "Bye bye :D" )
        exit(0)
      else
        exit( 1)
      end
    end
    def interface_for_converting_spotify_to_youtube_playlist
      puts "What playlist/album would you like to convert? To see how to do it"
      puts HELP_STRING
      collection_of_answers = PROMPT.collect do
        key(:link_of_playlist)
          .ask("\nPass link to playlist/album:", required: true)

        key(:playlist_info) do
          key(:title_of_playlist).ask("How would you like to name your playlist?", required: true)
          key(:description_of_playlist).ask("Would you like to give it a description?(optional - click enter to skip)")
        end
      end
      response = @client.convert_spotify_playlist_into_youtube_playlist(
        url_of_playlist: collection_of_answers[:link_of_playlist],
        title: collection_of_answers[:playlist_info][:title_of_playlist],
        description: collection_of_answers[:playlist_info][:description_of_playlist])

      if !response[0]
        main_prompt
        box(what_type_of_box: 'error', caption_inside: response[1])
      end
      box(what_type_of_box: 'completion', caption_inside: "Link to created playlist:\n #{response[1]}") if response[0]
      PROMPT.keypress("\nPress any key to continue(except esc of course)")
    end

    def interface_for_random_meme
      loop do
        main_prompt
        response = @client.random_meme_from_youtube
        if !response[0]
          box(what_type_of_box: "error", caption_inside: response[1])
          break
        else
          puts TTY::Link.link_to(response[1][:title], response[1][:url])
          break if PROMPT.keypress(INFORMATION_ABOUT_GENERATING) != 'a'
        end
      end
    end
    def interface_for_recommended_music
      loop do
        main_prompt
        response = @client.random_recommended_music
        if !response[0]
          box(what_type_of_box: "error", caption_inside: response[1])
          break if PROMPT.keypress('Press any key to continue')
        else
          puts TTY::Link.link_to("#{response[1][:title]} - #{response[1][:authors]}", response[1][:preview_url])
          break if PROMPT.keypress(INFORMATION_ABOUT_GENERATING) != 'a'
        end
      end
    end
    def help
      page = 0
      loop do
        clean_screen

        box(what_type_of_box: "info", caption_inside: INFORMATION[page])
        choice = PROMPT.slider("\n\nWhat page you want to display? "+
                                 "If you want to leave, move slider to the end or press CTRL+B") do |range|
          range.choices (1..INFORMATION.size).to_a + ['exit']
          range.format "|:slider| %s"
          range.default page + 1
        end
        if choice == 'exit'
          break
        else
          page = choice - 1
        end
      end
    end
    def choose_and_copy_converted_urls
      d = SavedUrl.new
      file_not_empty, urls_to_choice = d.reading_file_with_urls
      if !file_not_empty
        box(what_type_of_box: "error", caption_inside: urls_to_choice)
      else
        url = PROMPT.select("Choose url and press enter to copy the url to clipboard", urls_to_choice)
        string_splitted_by_spaces = url.split(" ")
        Clipboard.copy(string_splitted_by_spaces[0])
        box(what_type_of_box: 'completion',
            caption_inside: "Copied url of: " +
              "#{string_splitted_by_spaces[1..string_splitted_by_spaces.size - 1].join(" ")}")
      end
      PROMPT.keypress("\nPress any key to continue(except esc of course)")
    end
    private

    def help_column
      puts PASTEL.magenta("Note that in smaller resolutions of terminal application might " +
                            "not work so well (url forced cutting, not readable words)") +
             PASTEL.red("\nPress ESC to quit") + PASTEL.green("\tPress CTRL+B to go back to main_menu\n")
    end
    def main_prompt
      @main_prompt = MainPrompt.new if @main_prompt.nil?
      @main_prompt.display_main_prompt
    end

    def box(what_type_of_box:, caption_inside:)
      centered_in_width = TTY::Screen.width / 5
      centered_in_height = TTY::Screen.height / 2
      if TTY::Screen.width >= 200
        centered_in_width = TTY::Screen.width / 3
        centered_in_height = TTY::Screen.height / 4
      end
      if TTY::Screen.width <= 80
        centered_in_width = 0
      end
      case what_type_of_box
      when "info"
        print TTY::Box.info caption_inside,top: centered_in_height, left: centered_in_width
      when "error"
        puts TTY::Box.error(caption_inside, top: centered_in_height, left: centered_in_width)
      when "completion"
        puts TTY::Box.success(caption_inside, top: centered_in_height, left: centered_in_width)
      end
    end

    def clean_screen
      system ("clear") || system("cls")
    end
    def banner
      centered = TTY::Screen.width / 3
      tab = CURSOR.move(centered, 0)
      clean_screen
      puts tab + PASTEL.green(".oooooo..o") + PASTEL.red(" oooooo   oooo ") + PASTEL.magenta("oooo    oooo")
      puts tab + PASTEL.green("d8P'    `Y8  ") + PASTEL.red("`888.   .8'  ") + PASTEL.magenta("`888   .8P'  ")
      puts tab + PASTEL.green("Y88bo.        ") + PASTEL.red("`888. .8'    ") + PASTEL.magenta("888  d8'")
      puts tab + PASTEL.green("`\"Y8888o.     ") + PASTEL.red("`888.8'     ") + PASTEL.magenta("88888[      ")
      puts tab + PASTEL.green("     `\"Y88b     ") + PASTEL.red("`888'      ") + PASTEL.magenta("888`88b.    ")
      puts tab + PASTEL.green("oo     .d8P      ") + PASTEL.red("888       ") + PASTEL.magenta("888  `88b.")
      puts tab + PASTEL.green(" 8\"\"88888P'     ") + PASTEL.red("o888o     ")+ PASTEL.magenta("o888o  o888o ")
      puts "\n\n"
      puts tab + PASTEL.green("spotify\t   ") + PASTEL.red("youtube\t") + PASTEL.magenta("konwerter")
    end
  end
end
