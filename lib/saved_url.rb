class SavedUrl
  def reading_file_with_urls
    if File.exists?('generated_urls.txt')
      urls = []
      File.foreach("generated_urls.txt") { |url| urls.append(url) if !url.to_s.strip.empty? }
      [true, urls]
    else
      [false, "Nothing was generated or file was deleted :("]
    end
  end
  def writing_to_file(url:, description:)
    if File.exists?('generated_urls.txt')
      File.write("generated_urls.txt", "\n#{url} #{description}", mode: "a")
    else
      File.write("generated_urls.txt", "#{url} #{description}", mode: "w")
    end
  end
end
