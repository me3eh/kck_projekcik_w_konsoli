# require "rest-client"
require 'json'
require 'google/apis'
require 'google/apis/youtube_v3'
require 'googleauth'
require 'googleauth/stores/file_token_store'
require 'fileutils'
require 'net/http'
require 'uri'
require 'active_support/all'
require_relative '../config/init'
require_relative 'main_prompt'

REDIRECT_URI = 'http://localhost'
APPLICATION_NAME = 'YouTube Data API Ruby Tests'
# REPLACE WITH NAME/LOCATION OF YOUR client_secrets.json FILE
CLIENT_SECRETS_PATH = 'client_secret.json'
# REPLACE FINAL ARGUMENT WITH FILE WHERE CREDENTIALS WILL BE STORED
# CREDENTIALS_PATH = File.join(Dir.home, '.credentials', "youtube-quickstart-ruby-credentials.yaml")
CREDENTIALS_PATH = "youtube-quickstart-ruby-credentials.yaml"
# SCOPE FOR WHICH THIS SCRIPT REQUESTS AUTHORIZATION
SCOPE = Google::Apis::YoutubeV3::AUTH_YOUTUBE

# Initialize the API
class Yt_client
  PLAYLIST_ID = 'PLtiqACjksZjnntSKp6kySgRFzo9lmL17v'
  def initialize
    @array_of_memes = []
  end

  def create_yt_playlist(description = 'Nothing unnatural, move along people, move along', title:)
    access_token_and_client_id = get_access_token_and_client_id
    return [false, access_token_and_client_id[1]] if !access_token_and_client_id[0]
    access_token = access_token_and_client_id[1]
    client_id = access_token_and_client_id[2]

    uri = URI.parse("https://youtube.googleapis.com/youtube/v3/playlists?part=snippet%2Cstatus&key=#{client_id}")
    request = Net::HTTP::Post.new(uri)
    request.content_type = "application/json"
    request["Authorization"] = "Bearer #{access_token}"
    request["Accept"] = "application/json"
    request.body = JSON.dump({
                               "snippet" => {
                                 "title" => "#{title}",
                                 "description" => "#{description}",
                                 "tags" => [
                                   "sample playlist",
                                   "API call"
                                 ],
                                 "defaultLanguage" => "en"
                               },
                               "status" => {
                                 "privacyStatus" => "public"
                               }
                             })
    req_options = {
      use_ssl: uri.scheme == "https",
    }

    response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
      http.request(request)
    end
    response = JSON.parse(response.body)
    if response["error"]
      LOGGR.error("#{response["error"]["message"]} - creating playlist in method get_tracks_from_url\n" +
                    "in spotify_client.rb:24")
      return [false, response["error"]["message"]]
    else
      return [true, response["id"]]
    end
  end
  def search_track(full_name_of_track:)
    access_token_and_client_id = get_access_token_and_client_id
    return [false, access_token_and_client_id[1]] if !access_token_and_client_id[0]
    access_token = access_token_and_client_id[1]
    client_id = access_token_and_client_id[2]

    query = {:q => full_name_of_track}.to_query
    uri = URI.parse("https://youtube.googleapis.com/youtube/v3/search?maxResults=1&#{query}&key=#{client_id}")
    request = Net::HTTP::Get.new(uri)
    request["Authorization"] = "Bearer #{access_token}"
    request["Accept"] = "application/json"

    req_options = {
      use_ssl: uri.scheme == "https",
    }

    response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
      http.request(request)
    end

    response = JSON.parse(response.body)
    # puts response
    LOGGR.info(response)

    return [false, response["error"]["message"]] if response["error"]
    [true, response["items"][0]["id"]["videoId"]]
  end

  def add_to_playlist(playlist_id:, track_id:)
    access_token_and_client_id = get_access_token_and_client_id
    return [false, access_token_and_client_id[1]] if !access_token_and_client_id[0]
    access_token = access_token_and_client_id[1]
    client_id = access_token_and_client_id[2]

    uri = URI.parse("https://youtube.googleapis.com/youtube/v3/playlistItems?part=snippet&key=#{client_id}")
    request = Net::HTTP::Post.new(uri)
    request.content_type = "application/json"
    request["Authorization"] = "Bearer #{access_token}"
    request["Accept"] = "application/json"
    request.body = JSON.dump({
                               "snippet" => {
                                 "playlistId" => "#{playlist_id}",
                                 "position" => 0,
                                 "resourceId" => {
                                   "kind" => "youtube#video",
                                   "videoId" => "#{track_id}"
                                 }
                               }
                             })

    req_options = {
      use_ssl: uri.scheme == "https",
    }

    response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
      http.request(request)
    end
    LOGGR.info(response)
  end
  def random_meme
    url_template = 'https://www.youtube.com/watch?v='
    if @array_of_memes.empty?

      Yt::Playlist.new(id: PLAYLIST_ID).playlist_items.each do |video|
        @array_of_memes.append({ url: url_template + video.video_id,
                                 title: video.title})
      end
    end
    selected_video = @array_of_memes[rand @array_of_memes.size]
    [true, selected_video]
  end

  private

  def authorize
    FileUtils.mkdir_p(File.dirname(CREDENTIALS_PATH))
    client_id = Google::Auth::ClientId.from_file(CLIENT_SECRETS_PATH)
    token_store = Google::Auth::Stores::FileTokenStore.new(file: CREDENTIALS_PATH)
    authorizer = Google::Auth::UserAuthorizer.new(client_id, SCOPE, token_store)
    user_id = 'default'
    credentials = authorizer.get_credentials(user_id)
    if credentials.nil?
      url = authorizer.get_authorization_url(base_url: REDIRECT_URI)
      puts "Open the following URL in the browser and enter the resulting code after authorization"
      puts url
      code = STDIN.gets.chomp
      credentials = authorizer.get_and_store_credentials_from_code(
        user_id: user_id, code: code, base_url: REDIRECT_URI)
    end
    credentials
  end
  def get_access_token_and_client_id
    begin
      service = Google::Apis::YoutubeV3::YouTubeService.new
      service.client_options.application_name = APPLICATION_NAME
      authorization = authorize
      service.authorization = authorization
      return [true, authorization.fetch_access_token["access_token"], authorization.client_id]
    rescue => e
      LOGGR.error("#{e} - authorize in method get get_access_token_and_client_id\n" +
                    "in yt_client.rb:36")
      return [false, "Authorization failed. Check if you have client_secret.json and \n"+
        "youtube-quickstart-ruby-credentials.yaml in your project folder"]
    end
  end
end
