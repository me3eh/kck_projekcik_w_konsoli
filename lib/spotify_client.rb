require "rspotify"
require "dotenv"
Dotenv.load('.env')

class Spotify_client
  def initialize
    @array_of_recommended_songs = []
  end
  def authenticate
      RSpotify.authenticate(ENV["SPOTIFY_CLIENT_ID"].to_s, ENV["SPOTIFY_CLIENT_SECRET"].to_s)
  end
  def get_tracks_from_url(url_of_playlist:)
    begin
      authenticate
    rescue => e
      LOGGR.error("#{e} - authenticating in method get_tracks_from_url\n" +
                    "in spotify_client.rb:57")
      return [false, "Not authenticated. Look if you have .env\n file in your main directory of project\n" +
        "and try again :D"]
    end
  #/https:\/\/open\.spotify\.com\/playlist\/[A-Za-z0-9=]*\?si=[a-zA-Z0-9]/ that regex matches example url below
  # https://open.spotify.com/playlist/61HjYEXu0F7EqmI0MHvoYB?si=6e1452b365844247
    if url_of_playlist.include?("playlist")
      if !(url_of_playlist =~ /https:\/\/open\.spotify\.com\/playlist\/[A-Za-z0-9=]*\?si=[a-zA-Z0-9 _\-=]/)
        return [false, 'You did not provide link to the playlist']
      end
      id_of_playlist = url_of_playlist.split('playlist/')[1].split('?si=')[0]
      begin
        playlist = RSpotify::Playlist.find_by_id(id_of_playlist)
      rescue => e
        LOGGR.error("#{e} - finding playlist in method get_tracks_from_url\n" +
                      "in spotify_client.rb:24")
        return [false, "Could not find provided playlist.\nCheck the link for mispells and try again"]
      end
    else
      if !(url_of_playlist =~ /https:\/\/open\.spotify\.com\/album\/[A-Za-z0-9=]*\?si=[a-zA-Z0-9 _\-=]/)
        return [false, 'You did not provide link to the album']
      end
      id_of_playlist = url_of_playlist.split('album/')[1].split('?si=')[0]
      begin
        playlist = RSpotify::Album.find(id_of_playlist)
      rescue => e
        LOGGR.error("#{e} - finding album in method get_tracks_from_url\n" +
                      "in spotify_client.rb:36")
        return [false, "Could not find provided album.\n
                        Check the link for mispells and try again"]
      end
    end
    array_of_tracks = []

    playlist.tracks.each do |track|
      string_containing_artists = ""
      track.artists.each do |artist|
        string_containing_artists += artist.name + "\b "
      end
      array_of_tracks.append(track.name + " - " + string_containing_artists)
    end
    [true, array_of_tracks]
  end
  def get_random_track_from_recommended_playlist
    begin
      authenticate
    rescue => e
      LOGGR.error("#{e} - authenticating in method get_random_track_from_recommended_playlist\n" +
                    "in spotify_client.rb:57")
      return [false, "Not authenticated. Look if you have .env\n file in your main directory of project\n" +
        "and try again :D"]
    end
    begin
      playlist_id = '7Et1zsKFnBeV4dbtMtYq3t'
      if @array_of_recommended_songs.empty?
        RSpotify::Playlist.find_by_id(playlist_id).tracks.each do |track|
          string_containing_artists = ''
          track.artists.each do |artist|
            string_containing_artists += artist.name + "\b "
          end
          @array_of_recommended_songs.append({title: track.name,
                                              authors: string_containing_artists,
                                              preview_url: track.preview_url})
        end
      end
      return [true, @array_of_recommended_songs[ rand @array_of_recommended_songs.size] ]
    rescue => e
      LOGGR.error("#{e} - random track in method get_random_track_from_recommended_playlist\n" +
                    " in spotify_client.rb:76")
      return [false, "Something went wrong with generating \nrandom music reccomendation"]
    end
  end
end
