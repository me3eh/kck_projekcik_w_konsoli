# SYK

Welcome to SYK! Spotify Youtube Konwerter. 

## Installation

To setup project you need to execute 

    $ make setup

To properly use application, you also need 
- client_secret.json, 
- .env  
- youtube-quickstart-ruby-credentials.yaml

and put them into main folder (with project).
##Usage
To run application simply run 

    $ make run
To keep in mind, usage of 1 option (converting youtube to spotify playlist) is limited by quota amount, so you will 
need to swap client_secret.json and youtube-quickstart-ruby-credentials.yaml from time to time (when prompt will be 
visible on terminal screen). Unfortunately, Google is in possesion of this, so nothing more than writing a request to
them could be made in this situation. 
If you saw a problem with quota exceed, simply change file ```client_secret.json``` and ```generated_urls.txt``` with
another file and you're ready to go.
