require 'tty-cursor'
require 'tty-prompt'
require 'dotenv'
require 'pastel'
require 'tty-box'
require 'tty-link'
require 'yt'
require 'tty-progressbar'
require 'logger'
Dotenv.load('.env')

LOGGR = Logger.new('bin/logs.txt')
LOGGR.level = Logger::INFO

PROMPT = TTY::Prompt.new(interrupt: :exit, active_color: :magenta)
CURSOR = TTY::Cursor
PASTEL = Pastel.new
Yt.configure do |config|
  config.api_key = ENV['YOUTUBE_CLIENT_ID']
end